<?php
//memanggil file file class yang telah dibuat
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

//ini untuk menampilkan Shep
$sheep = new animal("Shaun");

echo "Name : ".$sheep->name;
echo "<br>";
echo "Legs : ".$sheep->legs;
echo "<br>";
echo "Cold Blooded : ".$sheep->cold_blooded;

echo "<br><br>";

//ini untuk menampilkan Kodok
$kodok = new frog("Buduk");
echo "Name : ".$kodok->name;
echo "<br>";
echo "Legs : ".$kodok->legs;
echo "<br>";
echo "Cold Blooded : ".$kodok->cold_blooded;
echo "<br>";
echo $kodok->jump();

echo "<br>";

//ini untuk menampilkan kera
$sungkokong = new ape("Kera Sakti");
echo "Name : ".$sungkokong->name;
echo "<br>";
echo "Legs : ".$sungkokong->legs;
echo "<br>";
echo "Cold Blooded : ".$sungkokong->cold_blooded;
echo "<br>";
echo $sungkokong->yell();



?>